﻿using System;
using System.Collections.Generic;

namespace exercise_22_arrays_and_list
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            var movieTitles = new string [5]  {"diehard","tradingPlaces","goonies","platoon","chopper"};
            List<string> Resturaunts = new List<string> {"Mc Donalds","Burger King","KFC","Lone star","Breakers"};
            

            //prints array to screen on seperate lines
            Console.WriteLine (movieTitles[0]);
            Console.WriteLine (movieTitles[1]);
            Console.WriteLine (movieTitles[2]);
            Console.WriteLine (movieTitles[3]);
            Console.WriteLine (movieTitles[4]);
           
            //change first movie and 3rd 
            Console.WriteLine("Enter a new movie");
            movieTitles[0]= Console.ReadLine();
            Console.WriteLine("Enter another movie");
            movieTitles[2]=Console.ReadLine();
            //lenth of array
            Console.WriteLine("Length of Array:      {0,4}", movieTitles.Length);
            //sorts by alpabetical order
            Array.Sort(movieTitles);
            // re prints to scrren in alpabetical order in a joined string 
            Console.WriteLine(string.Join (",",movieTitles));
            //sorts list alpabeticly
            Resturaunts.Sort ();
            //joins list in a string
            Console.WriteLine(string.Join(",",Resturaunts));
            //removes kfc from list
            Resturaunts.Remove("KFC");
            // writes list to screen
            Console.WriteLine(string.Join(",",Resturaunts));

           
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
